const mongoose = require('mongoose');

const TaskSchema = mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      trim: true,
      index: { unique: true },
      minLength: 3,
    },
    description: {
      type: String,
      required: true,
      trim: true,
      minLength: 8,
    },
    tags: {
      type: [
        {
          type: String,
          trim: true,
        },
      ],
      required: true,
    },
    done: {
      type: Boolean,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model('Task', TaskSchema);
