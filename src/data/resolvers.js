const TaskModel = require('./models/TaskModel');

const { useTryCatch } = require('../helpers/withTryCatch');
const { withErrorCallback } = require('../helpers/withAsync');

const resolvers = {
  Query: {
    getTodoTasks: () => useTryCatch(TaskModel.find({ done: false })),
    getDoneTasks: async () => useTryCatch(TaskModel.find({ done: true })),
    getTask: async (_, { id }) => useTryCatch(TaskModel.findById(id)),
    findTasks: async (_, { tags }) => useTryCatch(TaskModel.find({ tags: { $in: tags } })),
  },
  Mutation: {
    createTask: async (_, { task }) => {
      try {
        const newTask = new TaskModel(task);
        newTask.id = newTask._id;
        newTask.done = false;

        return await newTask.save();
      } catch (error) {
        return withErrorCallback(error);
      }
    },
    updateTask: async (_, { id, task }) => {
      try {
        return await TaskModel.findOneAndUpdate({ _id: id }, task, { new: true });
      } catch (error) {
        return withErrorCallback(error);
      }
    },
    deleteTask: async (_, { id }) => {
      try {
        await TaskModel.findOneAndDelete({ _id: id });
        return true;
      } catch (error) {
        return withErrorCallback(error);
      }
    },
  },
};

export default resolvers;
