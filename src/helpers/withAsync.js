const { DUPLICATE_DB_CODE, DUPLICATE_DB_MESSAGE } = require('../constants');

const withErrorCallback = (error) => {
  console.error(error);
  if (error.code && error.code === DUPLICATE_DB_CODE) {
    throw new Error(DUPLICATE_DB_MESSAGE);
  } else {
    throw new Error(error.message);
  }
};

module.exports = {
  withErrorCallback,
};
