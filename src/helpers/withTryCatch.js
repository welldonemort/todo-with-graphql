const { withErrorCallback } = require('./withAsync');

const useTryCatch = async (promise) => {
  try {
    return await promise;
  } catch (error) {
    return withErrorCallback(error);
  }
};

module.exports = { useTryCatch };
