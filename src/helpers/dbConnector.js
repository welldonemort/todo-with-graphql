const mongoose = require('mongoose');

const connectMongoDB = () => {
  mongoose.connect('mongodb://localhost/todo-gq');
  const db = mongoose.connection;
  db.on('error', (error) => console.log(error));
  db.once('open', () => {
    console.log('Database Connected...');
  });
};

export default connectMongoDB;
