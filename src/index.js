import { graphqlHTTP } from 'express-graphql';
import schema from './data/schema';
import { PORT, EXPRESS_SUCCESS_MESSAGE } from './constants';
import connectMongoDB from './helpers/dbConnector';

const express = require('express');

const app = express();
connectMongoDB();

app.use(
  '/graphql',
  graphqlHTTP({
    schema,
    graphiql: true,
  })
);

app.listen(PORT, () => console.log(EXPRESS_SUCCESS_MESSAGE));
