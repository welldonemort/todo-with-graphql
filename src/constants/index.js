export const PORT = 3000;
export const DUPLICATE_DB_CODE = 11000;

export const EXPRESS_SUCCESS_MESSAGE = `Express server listening on port http://localhost:${PORT}!`;
export const DUPLICATE_DB_MESSAGE = 'Task with this title already exists.';
